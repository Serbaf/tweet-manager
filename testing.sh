#!/bin/bash

failure=0

printf "\n\n"
printf "*********************************\n"
printf "* EXECUTING TESTS FOR MAIN FILE *\n"
printf "*********************************\n"

printf "\nGeneral tests: "
python3 -m tests.main_test
if [ $? -ne 0 ]; then
    failure=1
fi

printf "\nTests for split mode: "
python3 -m tests.split_test
if [ $? -ne 0 ]; then
    failure=1
fi

printf "\nTests for flat mode: "
python3 -m tests.flat_test
if [ $? -ne 0 ]; then
    failure=1
fi

printf "\nTests for singlejson2csv mode: "
python3 -m tests.singlejson2csv_test
if [ $? -ne 0 ]; then
    failure=1
fi

printf "\nTests for jsoncollection2csv mode: "
python3 -m tests.jsoncollection2csv_test
if [ $? -ne 0 ]; then
    failure=1
fi


printf "\n\n"
printf "****************************************\n"
printf "* EXECUTING TESTS FOR JSON2CSV LIBRARY *\n"
printf "****************************************\n"

printf "\nTests for lib/json2csv.py: "
python3 -m tests.json2csv_test
if [ $? -ne 0 ]; then
    failure=1
fi

exit $failure
