# Execution times
Different measures of times for the operations done by json_to_csv.
3 times each

## Full conversion (split + flatten + convert to CSV)

diada2016.json (205 MB)
```
real	3m2,325s
real	2m59,691s
real	3m1,502s
```

ChicagoGeoTime_Brexit.json (516K)
```
real	0m0,692s
real	0m0,638s
real	0m0,650s
```


## Splitting only

diada2016.json (205 MB)
```
real	0m38,292s
real	0m38,510s
real	0m38,529s
```

ChicagoGeoTime_Brexit.json (516K)
```
real	0m0,176s
real	0m0,180s
real	0m0,178s
```


## Flattening only (using output from previous splitting)

diada2016.json (205 MB)
```
real	0m22,756s
real	0m22,733s
real	0m22,920s
```

ChicagoGeoTime_Brexit.json (516K)
```
real	0m0,231s
real	0m0,230s
real	0m0,231s
```

## CSV conversion only (using output from previous flattening)

diada2016.json (205 MB)

```
real	1m55,526s
real	1m55,314s
real	1m55,661s
```

ChicagoGeoTime_Brexit.json (516K)
```
real	0m0,320s
real	0m0,310s
real	0m0,317s
```

## Observations
CSV conversion is clearly the heaviest part with quite a difference,
followed by splitting and finally flattening (although with a smaller
collection these two changed the order). If something had to be done, it
will better focus first on the CSV converting algorithm. Perhaps try to
find some optimizations or implement that critical part of the code in a
faster language.

Finally, note that the three execution parts by themselves would sum up
to something like 2m56s, while when everything is executed together it
takes around 3m1s. So the other parts of the code seem to take around
5s.
