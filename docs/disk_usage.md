# Disk usage
Total disk memory usage of the converted files

## Split collection

Original: diada2016.json (205 MB)
```
287 MB
```

Original: ChicagoGeoTime\_Brexit.json (516K)
```
4.5 MB
```


## Flat JSON collection

diada2016.json (205 MB)
```
389 MB
```

ChicagoGeoTime\_Brexit.json (516K)
```
4.6 MB
```

## CSV collection

diada2016.json (205 MB)

```
149 MB
```

ChicagoGeoTime\_Brexit.json (516K)
```
4.6 MB
```

## Observations
It seems that splitting the files adds some overhead to the original
collection, and flattening them adds even more, to the point of almost
doubling the total size. However, the process of passing to CSV reduces
significantly the memory usage.
It has to be noted that there are lots of "None", "Null" and "False"
values that add a significant weight to the collections in the CSV
phase, so probably this memory usage could be reduced even more.

The ChicagoGeoTime collection, much smaller, seems to show almost no
differences between formats.
